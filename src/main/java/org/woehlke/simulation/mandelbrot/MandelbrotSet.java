package org.woehlke.simulation.mandelbrot;

import java.io.Serializable;

/**
 * Mandelbrot Set drawn by a Turing Machine.
 *
 * (C) 2006 - 2015 Thomas Woehlke.
 * https://thomas-woehlke.blogspot.com/2016/01/mandelbrot-set-drawn-by-turing-machine.html
 * @author Thomas Woehlke
 *
 * Date: 17.11.2018
 * Time: 18:36:28
 */
public interface MandelbrotSet extends Serializable {

    long mySerialVersionUID = 242L;
}
