# Mandelbrot Set

* Fractals: Computing the Mandelbrot Set with a Turing Machine
* Blog: [mandelbrot set drawn by turing machine](http://thomas-woehlke.blogspot.de/2016/01/mandelbrot-set-drawn-by-turing-machine.html)
* Github Repository: [github.com/thomaswoehlke/mandelbrot](https://github.com/thomaswoehlke/mandelbrot)

## Abstract
The Mandelbrot Set is the set of values of c in the complex plane for which the orbit of 0 under iteration of the complex quadratic polynomial z_(n+1)=z_n^2+c remains bounded.
That is, a complex number c is part of the Mandelbrot set if, when starting with z0 = 0 and applying the iteration repeatedly, the absolute value of zn remains bounded however large n gets.

Sources: 
* [Wikipedia: Mandelbrot Set](https://en.wikipedia.org/wiki/Mandelbrot_set)

## Run the Desktop Application

```
    git clone https://github.com/phasenraum2010/mandelbrot.git
    cd mandelbrot
    ./gradlew run
```

## Screenshots: The Mandelbrot Set

### Screen 1
[Figure 1: Moving around the Mandelbrot Set](img/screen01.png)

### Screen 2
[Figure 2: Moving around the Mandelbrot Set](img/screen02.png)

### Screen 3
[Figure 3: Done](img/screen03.png)

## Screenshots: Julia Sets

### Julia Set 1
[Figure 4: Julia Set](img/julia01.png)

### Julia Set 2
[Figure 5: Julia Set](img/julia02.png)






